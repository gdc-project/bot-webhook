const functions = require("firebase-functions");
const { WebhookClient } = require("dialogflow-fulfillment");
const axios = require("axios");
const admin = require("firebase-admin");

admin.initializeApp();
const db = admin.firestore();

process.env.DEBUG = "dialogflow:debug";

exports.dialowflowfullfilment = functions.https.onRequest(
  (request, response) => {
    const agent = new WebhookClient({ request, response });

    function getDNIData(agent) {
      const apiURL = "https://apiperu.dev/api/dni";
      const headers = {
        Authorization: `Bearer e0d7521c02d1d042b63be1980d35ef5a7e1e8e9d8e18445db720df0ee0ff56c6`,
      };
      const dni = agent.parameters.document;

      return axios
        .get(`${apiURL}/${dni}`, { headers })
        .then((response) => {
          console.log("RENIEC DATA", JSON.stringify(response.data));
          const citizen = response.data.data;
          console.log("CITIZEN", JSON.stringify(citizen));

          if (citizen !== undefined) {
            const citizenParams = {
              dni: dni,
              names: citizen.nombres,
              surnames: `${citizen.apellido_paterno} ${citizen.apellido_materno}`,
            };
            agent.context.set(
              "awaiting_claim_register_document_confirm",
              1,
              citizenParams
            );
            agent.add(`¿Te llamas ${citizen.nombres}?`);
          } else {
            agent.context.set("awaiting_claim_register_document", 1);
            agent.add(
              `El DNI ${dni} no ha sido encontrado. Ingresa uno valido.`
            );
          }

          return true;
        })
        .catch((error) => {
          console.log("ERROR", JSON.stringify(error));
          agent.context.set("awaiting_claim_register_document", 1);
          agent.add(
            `Ocurrio un problema durante la consulta. Ingresa nuevamente tu DNI.`
          );
          return false;
        });
    }

    function storeCitizen(dni, names, surnames) {
      const citizensRef = db.collection("citizens").doc(`${dni}`);
      return citizensRef.set(
        { names: names, surnames: surnames },
        { merge: true }
      );
    }

    function confirmCitizen(agent) {
      const citizen = agent.contexts.find(
        (context) => context.name === "awaiting_claim_register_document_confirm"
      ).parameters;

      console.log(
        `DNI: ${citizen.dni} Nombres: ${citizen.names} Apellidos: ${citizen.surnames}`
      );

      agent.add(
        `¡Genial ${citizen.names}! ahora bríndame tu correo electrónico por favor.`
      );
      return storeCitizen(citizen.dni, citizen.names, citizen.surnames);
    }

    function storeIncident(incident, codeDate) {
      console.log("DOCUMENTO", JSON.stringify(incident.document));

      const citizenRef = db.collection("citizens").doc(incident.document);
      const incidentRef = db.collection("incidents");

      const request = {
        dni: incident.document,
        citizen: citizenRef,
        address: incident.address,
        district: incident.district,
        channel: "Bot",
        code: codeDate,
        registerDate: admin.firestore.FieldValue.serverTimestamp(),
        description: incident.description,
        status: "new",
        email: incident.email,
        //phone: incident.phone,
        type: "Reclamo",
      };

      console.log("REQUEST", JSON.stringify(request));
      return incidentRef.add(request);
    }

    function saveIncident() {
      console.log("CONTEXTOS", JSON.stringify(agent.contexts));
      const incident = agent.contexts.find(
        (context) => context.name === "followup_claim_register"
      ).parameters;

      console.log("PARAMS", JSON.stringify(incident));

      const code = Date.now().toPrecision();
      console.log("CODE", code);

      storeIncident(incident, code);
      agent.add(
        `Tu reclamo fue registrado, el código de incidencia es: ${code}`
      );
    }

    function getClaim(agent) {
      const code = agent.parameters.code;
      console.log("CODE", code);
      return db
        .collection("incidents")
        .where("code", "==", `${code}`)
        .get()
        .then((querySnapshot) => {
          let found = null;
          querySnapshot.forEach((doc) => {
            console.log("FOUND", JSON.stringify(doc.data()));
            found = doc.data();
          });

          let state = "";
          if (found) {
            switch (found.status) {
              case "new":
                state =
                  "Tu reclamo aun es nuevo y no ha sido asignado a nadie por el momento.";
                break;
              case "canceled":
                state =
                  "Tu reclamo fue cancelado porque no tiene relación con la Municipalidad Local.";
                break;
              case "assigned":
                state =
                  "Tu reclamo ya fue asigando a un asesor y esta siendo revisado.";
                break;
              case "attended":
                state =
                  "Tu reclamo ya tiene una respuesta y ahora esta siendo revisado por el gerente para asegurar la calidad de la misma";
                break;
              case "finished":
                state =
                  "Tu reclamo fue revisado por el gerente y pronto se te notificará al correo";
                break;
              case "notified":
                state =
                  "Tu reclamo ya fue notificado, buscalo en tu bandeja de correo.";
                break;
            }

            agent.add(state);
          } else {
            agent.add(
              `Tu reclamo no fue encontrado. Verifica tu código e ingresalo nuevamente.`
            );
          }

          return true;
        })
        .catch((error) => {
          console.log("ERROR", JSON.stringify(error));
          agent.context.set("awaiting_claim_search_document", 1);
          agent.add(
            `Tu reclamo no fue encontrado. Verifica tu código e ingresalo nuevamente.`
          );
          return false;
        });
    }

    let intentMap = new Map();
    intentMap.set("Claim - Register - DNI", getDNIData);
    intentMap.set("Claim - Register - DNI - Yes", confirmCitizen);
    intentMap.set("Claim - Register - Incident", saveIncident);

    intentMap.set("Claim - Consult - Code", getClaim);

    agent.handleRequest(intentMap);
  }
);
